function [train_data, train_label, validation_data, ...
    validation_label, test_data, test_label] = preprocess()
% preprocess function loads the original data set, performs some preprocess
%   tasks, and output the preprocessed train, validation and test data.

% Input:
% Although this function doesn't have any input, you are required to load
% the MNIST data set from file 'mnist_all.mat'.

% Output:
% train_data: matrix of training set. Each row of train_data contains 
%   feature vector of a image
% train_label: vector of label corresponding to each image in the training
%   set
% validation_data: matrix of training set. Each row of validation_data 
%   contains feature vector of a image
% validation_label: vector of label corresponding to each image in the 
%   training set
% test_data: matrix of training set. Each row of test_data contains 
%   feature vector of a image
% test_label: vector of label corresponding to each image in the testing
%   set

% Some suggestions for preprocessing step:
% - divide the original data set to training, validation and testing set
%       with corresponding labels
% - convert original data set from integer to double by using double()
%       function
% - normalize the data to [0, 1]
% - feature selection

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   YOUR CODE HERE %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

train = load('mnist_all.mat','train*');
fn = fieldnames(train);
train_data = [];
test_data = [];
validation_data = [];

for indx = 1: length(fn)
    [m n] = size(train.(fn{indx}));
    trainingSize = round(m * 5/6);
    validationSize = m - trainingSize;
    splitdata = mat2cell(train.(fn{indx}),[trainingSize validationSize],n);
    if isempty(train_data)
        train_data = splitdata{1};
        train_label = (ones(trainingSize,1));
    else
        train_data = vertcat(train_data,splitdata{1});
        train_label = vertcat(train_label,indx*(ones(trainingSize,1)));
    end 
    if isempty(validation_data)
        validation_data = splitdata{2};
        validation_label = (ones(validationSize,1));
    else
        validation_data = vertcat(validation_data,splitdata{2});
        validation_label = vertcat(validation_label,indx*(ones(validationSize,1)));
    end
end

test = load('mnist_all.mat','test*');
fn = fieldnames(test);

for indx = 1: length(fn)
    [m n] =size(test.(fn{indx}));
    if isempty(test_data)
        test_data = test.(fn{indx});
        test_label = (ones(m,1));
    else
        test_data = vertcat(test_data,test.(fn{indx}));
        test_label = vertcat(test_label,indx*(ones(m,1)));
    end 
end

train_data = double(train_data);
validation_data = double(validation_data);
test_data = double(test_data);

train_data = train_data./255;
validation_data = validation_data./255;
test_data = test_data./255;

dev_val = std(train_data,0,1);
[r ,c] = find(dev_val == 0);
train_data(:,c) = [];
test_data(:,c) = [];
validation_data(:,c) = [];

end

